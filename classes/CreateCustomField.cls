@RestResource (urlMapping = '/createCustomField')
global with sharing class CreateCustomField
{
    @HttpPost
    global static void createField(String customObjectName, String customFieldName, Boolean externalId, Boolean required)
    {
        MetadataService.MetadataPort service = createService();
        MetadataService.CustomField customField = new MetadataService.CustomField();
        customField.fullName = CustomObjectName+'.'+customFieldName;
        customField.label = customFieldName;
        customField.type_x = 'Text';
        customField.length = 50;

        List<MetadataService.SaveResult> results =
            service.createMetadata(
                new MetadataService.Metadata[] { customField });
        handleSaveResults(results[0]);
        updateFieldLevelSecurity(customObjectName, customFieldName);
        if(required){
            updateFieldAsRequired(customObjectName, customFieldName);
        }
        if(externalId){
            updateFieldAsExternalId(customObjectName, customFieldName);
        }
    }

    public class CreateCustomFieldException extends Exception { }

    public static MetadataService.MetadataPort createService()
    {
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = UserInfo.getSessionId();
        return service;
    }

    public static void updateFieldAsRequired(String customObjectName, String customFieldName)
    {
        MetadataService.MetadataPort service = createService();
        MetadataService.CustomField customField = new MetadataService.CustomField();
        customField.fullName = CustomObjectName+'.'+customFieldName;
        customField.label = customFieldName;
        customField.type_x = 'Text';
        customField.length = 50;
        customField.required= true;

        List<MetadataService.SaveResult> results =
            service.updateMetadata(
                new MetadataService.Metadata[] { customField });
        handleSaveResults(results[0]);
    }

 public static void updateFieldAsExternalId(String customObjectName, String customFieldName)
    {
        MetadataService.MetadataPort service = createService();
        MetadataService.CustomField customField = new MetadataService.CustomField();
        customField.fullName = CustomObjectName+'.'+customFieldName;
        customField.label = customFieldName;
        customField.type_x = 'Text';
        customField.length = 50;
        customField.externalId = true;

        List<MetadataService.SaveResult> results =
            service.updateMetadata(
                new MetadataService.Metadata[] { customField });
        handleSaveResults(results[0]);
    }

    public static void updateFieldLevelSecurity(String customObjectName, String customFieldName)
    {
        MetadataService.MetadataPort service = createService();
        MetadataService.Profile admin = new MetadataService.Profile();
        admin.fullName = 'Admin';
        admin.custom = false;
        MetadataService.ProfileFieldLevelSecurity fieldSec = new MetadataService.ProfileFieldLevelSecurity();
        fieldSec.field=customObjectName+'.'+customFieldName;
        fieldSec.editable=true;
        admin.fieldPermissions  = new MetadataService.ProfileFieldLevelSecurity[] {fieldSec} ;
        List<MetadataService.SaveResult> results =
            service.updateMetadata(
                new MetadataService.Metadata[] { admin });
        handleSaveResults(results[0]);
    }


    public static void handleSaveResults(MetadataService.SaveResult saveResult)
    {
        if(saveResult==null || saveResult.success)
            return;
        if(saveResult.errors!=null)
        {
            List<String> messages = new List<String>();
            messages.add(
                (saveResult.errors.size()==1 ? 'Error ' : 'Errors ') +
                    'occured processing component ' + saveResult.fullName + '.');
            for(MetadataService.Error error : saveResult.errors)
                messages.add(
                    error.message + ' (' + error.statusCode + ').' +
                    ( error.fields!=null && error.fields.size()>0 ?
                        ' Fields ' + String.join(error.fields, ',') + '.' : '' ) );
            if(messages.size()>0)
                throw new CreateCustomFieldException(String.join(messages, ' '));
        }
        if(!saveResult.success)
            throw new CreateCustomFieldException('Request failed with no specified error.');
    }
}