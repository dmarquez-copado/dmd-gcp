@RestResource (urlMapping = '/deleteTestData/*')
global class DeleteTestData
{
    @HttpDelete
    global static void deleteTestData()
    {
       String prefix = RestContext.request.params.get('prefix');
       String filter= '%'+prefix+'%';
       deleteOpportunities(filter);
       deleteAccounts(filter);
       deleteContacts(filter);
       deletePriceBooks(filter);
       deletePriceBooksEntry(filter);
       deleteOpportunityProduct(filter);
       deactivateUsers(filter);
   }


   global static void deleteOpportunities(String filter)
   {

        Opportunity[] ops = [SELECT Id, Name FROM Opportunity WHERE Name LIKE :filter];
        try {
            delete ops;
        } catch (DmlException e) {
            // Process exception here
        }
   }

   global static void deleteAccounts(String filter)
    {

        Account[] accs = [SELECT Id, Name FROM Account WHERE Name LIKE :filter];
        try {
            delete accs;
        } catch (DmlException e) {
            // Process exception here
        }
   }
   global static void deleteContacts(String filter)
    {

        Contact[] accs = [SELECT Id, Name FROM Contact WHERE Name LIKE :filter];
        try {
            delete accs;
        } catch (DmlException e) {
            // Process exception here
        }
   }
global static void deletePriceBooks(String filter)
    {

        PriceBook2[] accs = [SELECT Id, Name FROM PriceBook2 WHERE Name LIKE :filter];
        try {
            delete accs;
        } catch (DmlException e) {
            // Process exception here
        }
   }
global static void deletePriceBooksEntry(String filter)
    {

        PriceBookEntry[] accs = [SELECT Id, Name FROM PriceBookEntry WHERE Name LIKE :filter];
        try {
            delete accs;
        } catch (DmlException e) {
            // Process exception here
        }
   }
global static void deleteOpportunityProduct(String filter)
    {

        OpportunityLineItem[] accs = [SELECT Id, Name FROM OpportunityLineItem WHERE Name LIKE :filter];
        try {
            delete accs;
        } catch (DmlException e) {
            // Process exception here
        }
   }

    global static void deleteProduct(String filter)
    {

        Product2[] accs = [SELECT Id, Name FROM Product2 WHERE Name LIKE :filter];
        try {
            delete accs;
        } catch (DmlException e) {
            // Process exception here
        }
   }
    @future
   global static void deactivateUsers(String filter)
    {
        list<User> usersToUpdate = new list<User>();
        User[] userList = [SELECT Id, Name FROM User WHERE Name LIKE :filter];
        for(User us : userList){
            us.IsActive=false;
            usersToUpdate.add(us);
        }
        if(!usersToUpdate.isEmpty())
            update usersToUpdate;
   }
 }