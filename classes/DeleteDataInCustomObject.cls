@RestResource (urlMapping = '/deleteDataInCustomObject/*')
global class DeleteDataInCustomObject
{
    @HttpDelete
    global static void deleteDataInObject()
    {
        String customObjectName = RestContext.request.params.get('customObjectName');
        deleteAllRegisters(customObjectName);
    }

     public static void deleteAllRegisters(String tableName)
    {
        try {
            delete runQuery(tableName);
        } catch (DmlException e) {

        }
    }

    public static List<sObject> runQuery(String objectName)
    {
        List<sObject> queryresult;

        String soqlquery = 'Select Id from '+objectName+'';
        queryresult = Database.query(soqlquery);
        return queryresult;
    }
}