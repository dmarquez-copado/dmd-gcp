@RestResource (urlMapping = '/updateProfile')
global with sharing class UpdateProfile
{
    @HttpPost
    global static void updateProfile(String recordType)
    {
        MetadataService.MetadataPort service = createService();
        MetadataService.Profile admin = new MetadataService.Profile();
        admin.fullName = 'Admin';
        admin.custom = false;
        MetadataService.ProfileRecordTypeVisibility rt = new MetadataService.ProfileRecordTypeVisibility();

        rt.recordType=recordType;
        rt.visible=true;
        //rt.personAccountDefault=true;
        rt.default_x=true;
        admin.recordTypeVisibilities  = new MetadataService.ProfileRecordTypeVisibility[] {rt} ;
        List<MetadataService.SaveResult> results =
            service.updateMetadata(
                new MetadataService.Metadata[] { admin });
        handleSaveResults(results[0]);

     /*   List<String> availableCaseTypes = GetAvailableRecordTypeNamesForSObject(AddCustomObject__c.SObjectType);

         System.debug('[SANTI]'+ availableCaseTypes);*/
    }

    public static MetadataService.MetadataPort createService()
    {
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = UserInfo.getSessionId();
        return service;
    }

   public static void handleSaveResults(MetadataService.SaveResult saveResult)
    {
        if(saveResult==null || saveResult.success)
            return;
        if(saveResult.errors!=null)
        {
            List<String> messages = new List<String>();
            messages.add(
                (saveResult.errors.size()==1 ? 'Error ' : 'Errors ') +
                    'occured processing component ' + saveResult.fullName + '.');
            for(MetadataService.Error error : saveResult.errors)
                messages.add(
                    error.message + ' (' + error.statusCode + ').' +
                    ( error.fields!=null && error.fields.size()>0 ?
                        ' Fields ' + String.join(error.fields, ',') + '.' : '' ) );
            if(messages.size()>0)
                throw new updateProfileException(String.join(messages, ' '));
        }
        if(!saveResult.success)
            throw new updateProfileException('Request failed with no specified error.');
    }

    public class updateProfileException extends Exception{}


    public static List<String> GetAvailableRecordTypeNamesForSObject(Schema.SObjectType objType) {
    List<String> names = new List<String>();
    List<RecordTypeInfo> infos = objType.getDescribe().getRecordTypeInfos();

    if (infos.size() > 1) {
        for (RecordTypeInfo i : infos) {
           if (i.isAvailable()

            && !String.valueOf(i.getRecordTypeId()).endsWith('AAA'))
                names.add(i.getName());
        }
    }

    else names.add(infos[0].getName());
    return names;
}
}